<?php
 class Furniture extends Products{

    private $height;
    private $width;
    private $length;
    private $dimension;

    function __construct($data)
    {
        Parent::__construct($data);
        $this->height = $data['height'];
        $this->width = $data['width'];
        $this->length = $data['length'];
    }

    public function setHeight($height){
        $this->height = $height;
    }

    public function getHeight(){
        return $this->height;
    }

    public function setWidth($width){
        $this->width = $width;
    }

    public function getWidth(){
        return $this->width;
    }

    public function setLength($length){
        $this->length = $length;
    }

    public function getLength(){
        return $this->length;
    }

    public function getDimensions(){
        return $this->dimension;
    }

    public function validateAttributes(){
        if(is_numeric($this->height) && is_numeric($this->width) && is_numeric($this->length) && floatval($this->height >= 0) && floatval($this->width >= 0) && floatval($this->length >= 0))
        {
            return $this->dimension = $this->height.'x'.$this->width.'x'.$this->length;
        }
        return false;
    }

    public function validateAttributeName(){
        return $this->attributeName . 'Dimension';
    }

    public function addProduct(){
        $sql = "INSERT INTO " . $this->table . "(sku, name, price, type, attributeName, attribute) VALUES (?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->connect()->prepare($sql);
        $stmt->execute([$this->sku, $this->name, $this->price, $this->type, $this->validateAttributeName() ,$this->dimension]);
        header("location: index.php");
    }

 }
?>