<?php
    class Query{

        private $db;

        function __construct()
        {
            $this->db = new Database;
        }

        public function fetchProducts(){
            $sql = "SELECT * FROM products";
            $stmt = $this->db->connect()->prepare($sql);
            $stmt->execute();
            
            while($result = $stmt->fetchAll()){
                return $result;
            }

        }

        public function deleteProduct($sku){
            $sql = "DELETE FROM products WHERE `products`.`sku` = ? ";
            $stmt = $this->db->connect()->prepare($sql);
            $stmt->execute([$sku]);
            header("location: index.php");
        }

    }
?>