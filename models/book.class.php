<?php
class Book extends Products{
    private $weight;
    
    function __construct($data)
    {
        Parent::__construct($data);
        $this->weight = $data['weight'];
    }

    public function setWeight($weight){
        $this->weight = $weight;
    }
    public function getWeight(){
        return $this->weight;
    }
    
    public function validateAttributes(){
        if(is_numeric($this->weight) && floatval($this->weight >= 0))
        {
            return $this->weight = $this->weight .' KG';
        }
        return false;
    }

    public function validateAttributeName(){
        return $this->attributeName . 'Weight';
    }

    public function addProduct(){
        $sql = "INSERT INTO " . $this->table . "(sku, name, price, type, attributeName, attribute) VALUES (?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->connect()->prepare($sql);
        $stmt->execute([$this->sku, $this->name, $this->price, $this->type, $this->validateAttributeName() ,$this->weight]);
        header("location: index.php");
    }
    
}
?>