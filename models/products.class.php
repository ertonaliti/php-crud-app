<?php
    class Products{
        protected $table = 'products';
        protected $db;

        protected $sku;
        protected $name;
        protected $price;
        protected $type;
        protected $attributeName;

        function __construct($data)
        {
            $this->db = new Database;
            
            $this->sku = $data['sku'];
            $this->name = $data['name'];
            $this->price = $data['price'];
            $this->type = $data['productType'];
        }

        public function setSku($sku){
            $this->sku = $sku;
        }

        public function getSku(){
            return $this->sku;
        }
        public function setName($name){
            $this->name = $name;
        }

        public function getName(){
            return $this->name;
        }
        public function setPrice($price){
            $this->price = $price;
        }

        public function getPrice(){
            return $this->price;
        }
        public function setType($type){
            $this->type = $type;
        }

        public function getType(){
            return $this->type;
        }

        public function findSku(string $sku){
            $sql = "SELECT * FROM " . $this->table ." WHERE sku = ?";
            $stmt = $this->db->connect()->prepare($sql);
            $stmt->execute([$sku]);
            $result = $stmt->rowCount();
            if($result > 0){
                return $result;
            }
        }

        public function validateSKU()
        {
            return (!preg_match('/\s/', $this->sku) && !$this->findSku($this->sku) && (strlen($this->sku) > 0));
        }

        public function validateName(){
            return (strlen($this->name) > 0);
        }

        public function validateType(){
            return (strlen($this->type) > 0);
        }
        public function validatePrice(){
            return (filter_var($this->price, FILTER_VALIDATE_FLOAT) && (strlen($this->price) > 0) && floatval($this->price >= 0));
        }


    }
?>