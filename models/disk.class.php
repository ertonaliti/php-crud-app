<?php
class Disk extends Products{
    private $size;
    
    function __construct($data)
    {
        Parent::__construct($data);
        $this->size = $data['size'];
    }
    
    public function setSize($size){
        $this->size = $size;
    }

    public function getSize(){
        return $this->size;
    }
    
    public function validateAttributes(){
        if(is_numeric($this->size) && floatval($this->size >= 0))
        {  
            return $this->size = $this->size .' MB';
        }
        return false;
    }

    public function validateAttributeName(){
        return $this->attributeName . 'Size';
    }

    public function addProduct(){
        $sql = "INSERT INTO " . $this->table . "(sku, name, price, type, attributeName, attribute) VALUES (?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->connect()->prepare($sql);
        $stmt->execute([$this->sku, $this->name, $this->price, $this->type, $this->validateAttributeName() ,$this->size]);
        header("location: index.php");
    }
    
}
?>