function changeEvent(){
    var select = document.getElementById('productType');
    var value = select.options[select.selectedIndex].value;

    var Dvd = document.getElementById('Dvd');
    var Book = document.getElementById('Book');
    var Furniture = document.getElementById('Furniture');
    var Size = document.getElementById('size');
    var Weight = document.getElementById('weight');
    var Height = document.getElementById('height');
    var Width = document.getElementById('width');
    var Length = document.getElementById('length');

    if(value == 'Disk'){
        Dvd.style.display = 'flex';
        Book.style.display = 'none';
        Furniture.style.display = 'none';
        Size.disabled = false;
        Weight.disabled = true;
        Height.disabled = true;
        Width.disabled = true;
        Length.disabled = true;
    } else if(value == 'Book'){
        Dvd.style.display = 'none';
        Book.style.display = 'flex';
        Furniture.style.display = 'none';
        Size.disabled = true;
        Weight.disabled = false;
        Height.disabled = true;
        Width.disabled = true;
        Length.disabled = true;
    } else if(value == 'Furniture') {
        Dvd.style.display = 'none';
        Book.style.display = 'none';
        Furniture.style.display = 'inline';
        Size.disabled = true;
        Weight.disabled = true;
        Height.disabled = false;
        Width.disabled = false;
        Length.disabled = false;
    }else if(value == 'none'){
        document.getElementById('Dvd').style.display = 'none';
        document.getElementById('Furniture').style.display = 'none';
        document.getElementById('Book').style.display = 'none';
    }
}
changeEvent()